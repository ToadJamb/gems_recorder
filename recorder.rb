#!/usr/bin/ruby

require 'fileutils'
require 'time'

class Recorder
  class << self
    def path
      @path ||= File.expand_path(File.join('..', 'log'), __FILE__)
    end
  end

  @@file = File.join(path, 'recorder.log')
  @@sum_prefix = 'SUM '
  @@status = 'STATUS'

  def initialize(item)
    @item = item
  end

  def execute
    case @item
      when /^#{@@status}/
        show_status
      when /^#{@@sum_prefix}/
        summarize
      else append_file
    end
  end

  ############################################################################
  private
  ############################################################################

  def show_status
    item = nil
    File.open(@@file, 'r') do |file|
      while line = file.gets
        item = line
      end
    end
    puts item.sub(/,/, ', ')
  end

  def summarize
    item = @item.sub(/^#{@@sum_prefix}/, '')
    total = 0
    diff = false
    time = nil
    File.open(@@file, 'r') do |file|
      while line = file.gets
        line.chomp!
        if !diff && line.include?(item)
          next if item == line_date(line) && line =~ /,TODO$/
          diff = !diff
          time = to_time(line)
        elsif diff
          total = total + (to_time(line) - time)
          diff = !diff unless item == line_date(line) && line[-4..-1] != 'TODO'
          time = to_time(line) if diff
        end
      end
      total = total + (Time.now - time) if diff
    end
    puts format_total(total)
  end

  def line_date(line)
    line[0..line.index(' ') - 1]
  end

  def append_file
    File.open(@@file, 'a') { |file| file.puts Time.now.to_s + ',' + @item }
  end

  def to_time(line)
    Time.parse(line[0..line.index(',') - 1])
  end

  def format_total(secs)
    hours = (secs / (60 * 60)).to_i
    secs = secs - (hours * 60 * 60)
    min = (secs / 60).to_i
    secs = secs - (min * 60)
    return "#{hours}:#{min.to_s.rjust(2, '0')}:#{secs.to_i.to_s.rjust(2, '0')}"
  end
end

unless ARGV.empty?
  FileUtils.mkdir_p Recorder.path
  app = Recorder.new(ARGV.join(' '))
  app.execute
end
